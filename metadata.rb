name             'omnibus-gitlab'
maintainer       'GitLab B.V.'
maintainer_email 'jacob@gitlab.com'
license          'All rights reserved'
description      'Installs/Configures GitLab using omnibus-gitlab'
long_description 'Installs/Configures GitLab using omnibus-gitlab'
version          '0.2.0'

